SetTitleMatchMode, 2

#include apps.ahk
#include addons.ahk

~LAlt up::return
!`::cycleCurrentWindow()

!h::
WinMinimize, A
return

^!k::
if(A_PriorHotkey = "^!k") {
    if(A_TimeSincePriorHotkey < 250) {
        WinMaximize, A
        return
    }
}
Send ^!t
return
