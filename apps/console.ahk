$!\::
if WinActive("ahk_class Emacs") {
    Send !\
    return
}
else if WinExist("ahk_exe Hyper.exe")
{
    if WinActive("ahk_exe Hyper.exe")
    {
        WinMinimize, A
    }
    else
    {
        WinActivate
    }
}
else if WinExist("ahk_class ConsoleWindowClass")
{
    if WinActive("ahk_class ConsoleWindowClass")
    {
        WinMinimize, A
    }
    else
    {
        WinActivate
    }
}
else
{
    Run "powershell"
    WinWait, ahk_class ConsoleWindowClass
    WinActivate, ahk_class ConsoleWindowClass
}
return