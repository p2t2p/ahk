;;;; Switching tabs with only usable shortcut
$!+h::
SetTitleMatchMode, 2
if WinActive("Microsoft Edge")
{
    SendInput ^+{Tab}
}
else if WinActive("ahk_exe powershell_ise.exe")
{
    SendInput ^+{Tab}
}
else if WinActive("ahk_exe chrome.exe")
{
    SendInput ^+{Tab}
}
else
{
    SendInput !+h
}
SetTitleMatchMode, 1
return

$!+l::
SetTitleMatchMode, 2
if WinActive("Microsoft Edge")
{
    SendInput ^{Tab}
}
else if WinActive("ahk_exe powershell_ise.exe")
{
    SendInput ^{Tab}
}
else if WinActive("ahk_exe chrome.exe")
{
    SendInput ^{Tab}
}
else 
{
    SendInput !+l
}
SetTitleMatchMode, 1
return
